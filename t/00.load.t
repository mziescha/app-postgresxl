use Test::More tests => 1;

BEGIN {
use_ok( 'App::PostgresXL' );
}

diag( "Testing App::PostgresXL $App::PostgresXL::VERSION" );
